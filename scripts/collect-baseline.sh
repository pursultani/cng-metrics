#!/bin/bash

CNG_PROJECT_ID='4359271'
PIPELINES_BEFORE='2023-02-01'
MAX_PAGES='3'

query_cng() {
  local endpoint="${1}"
  local page_no="${2}"

  curl -f \
    -H 'Accept: application/json' \
    -H "Authorization: Bearer ${GITLAB_API_TOKEN}" \
    "${GITLAB_API_URL}/projects/${CNG_PROJECT_ID}/${endpoint}&per_page=100&page=${page_no}"
}

recent_successful_cng_pipelines() {
  local page_no="${1}"

  query_cng \
      "pipelines?updated_before=${PIPELINES_BEFORE}&status=success" \
      "${page_no}"
}

successful_pipeline_jobs() {
  local pipeline_id="${1}"

  query_cng \
      "pipelines/${pipeline_id}/jobs?scope=success"
}

HEADER='pipeline_id,job_id,name,stage,ref,source,duration,queued_duration,runner_id'
OUTPUT='data.csv'

echo ${HEADER} > ${OUTPUT}

for page_no in $(seq 1 ${MAX_PAGES}); do
  for pipeline_id in $(recent_successful_cng_pipelines "${page_no}" | jq -r '.[].id'); do
    successful_pipeline_jobs ${pipeline_id} | \
      jq -r '.[] | "\(.pipeline.id),\(.id),\(.name),\(.stage),\(.ref),\(.pipeline.source),\(.duration),\(.queued_duration),\(.runner.id)"' >> ${OUTPUT}
  done
done

